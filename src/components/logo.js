/** @jsxImportSource theme-ui */
import { Image, useColorMode } from "theme-ui";
import { Link } from "components/link";
import logo from "assets/fluxengine.svg";
import darklogo from "assets/fluxengine_dark.svg";

export default function Logo() {
  const [colorMode] = useColorMode();
  const icon = colorMode === "dark" ? darklogo : logo;
  return (
    <Link
      path="/"
      sx={{
        variant: "links.logo",
      }}
    >
      <Image
        src={icon}
        width="186"
        height="37"
        sx={{ display: "flex" }}
        alt="startup landing logo"
      />
    </Link>
  );
}
