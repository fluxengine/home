export default [
  {
    path: "banner",
    label: "Home",
  },
  {
    path: "services",
    label: "Services",
  },
  {
    path: "trust",
    label: "Our Values",
  },
  {
    path: "testimonials",
    label: "Testimonials",
  },
];
