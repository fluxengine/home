import React from "react";
import { Box, Container, Flex, Heading, Text } from "theme-ui";
import Image from "components/image";
import { IoIosCheckmarkCircle } from "react-icons/io";

import image1 from "assets/raccoon_rocket.jpeg";
import { maxHeight } from "@mui/system";

const BoostAgencies = () => {
  return (
    <Box sx={styles.boostAgencies}>
      <Container>
        <Box sx={styles.row}>
          <Flex sx={styles.col}>
            <Box sx={styles.content}>
              <Box sx={styles.titleBox}>
                <Heading as="h3">
                  Boost your productivity by choosing Flux Engine
                </Heading>
                <Text as="p">
                  We have a long history of providing expert guidance to a
                  diverse clientele exceeding 100 organizations. Our client base
                  spans across a spectrum, from multinational corporations
                  boasting thousands of employees to mid-sized enterprises and
                  burgeoning startups alike.
                </Text>
              </Box>
              <Box as="ul" sx={styles.list}>
                <Text as="li">
                  <IoIosCheckmarkCircle />
                  Insured up to $10 Million
                </Text>
                <Text as="li">
                  <IoIosCheckmarkCircle />
                  Data & Devops & Software Development
                </Text>
                <Text as="li">
                  <IoIosCheckmarkCircle />
                  Google Cloud Platform
                </Text>
              </Box>
            </Box>
          </Flex>
          <Flex sx={styles.col}>
            <Image
              src={image1}
              width="500"
              height="500"
              loading="lazy"
              sx={styles.image}
              alt=""
            />
          </Flex>
        </Box>
      </Container>
    </Box>
  );
};

export default BoostAgencies;

const styles = {
  boostAgencies: {
    pt: "5%",
    pd: "5%",
  },
  row: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: ["column", null, null, "row-reverse"],
  },
  col: {
    flex: ["0 0 100%", null, null, "0 0 50%"],
  },
  image: {
    display: "flex",
    marginLeft: "auto",
    marginRight: "auto",
    height: ["385px", null, null, "auto"],
    maxHeight: "auto",
    position: "relative",
    top: "auto",
    pb: "5%",
    "border-radius": "5%",
  },
  list: {
    margin: 0,
    padding: 0,
    listStyle: "none",
    ml: ["25px", null, null, "0"],
    mb: ["10px"],
    mt: ["30px"],
    li: {
      display: "flex",
      alignItems: "center",
      justifyContent: ["flex-start", null, null, null, null, "flex-start"],
      fontSize: [1, null, 2, null, "18px"],
      color: "text_secondary",
      lineHeight: [2.56],
      svg: {
        width: [17, null, null, 23],
        height: [17, null, null, 23],
        color: "#DADADA",
        borderRadius: "50%",
        marginRight: ["10px"],
      },
    },
  },
  titleBox: {
    textAlign: ["center", null, null, "left"],
    h3: {
      color: "black",
      fontSize: [5, null, null, "21px", "36px", "32px", 8],
      lineHeight: [1.6, null, null, "1.5"],
      fontWeight: "bold",
      letterSpacing: ["-0.5px", null, null, null, null, null, "-1.5px"],
    },
    p: {
      fontSize: [0, null, 2, null, "17px"],
      color: "text_secondary",
      opacity: ".6",
      lineHeight: ["26px", null, null, 1.8, null, 2.06],
      padding: ["0 20px", null, null, "0"],
      mt: ["15px"],
    },
  },
  link: {
    color: "primary",
    fontSize: [1, null, 2],
    display: "inline-block",
    verticalAlign: "middle",
    fontWeight: "bold",
    pl: ["30px", null, null, "4px", null, "4px"],
    mt: ["5px", null, null, null, "10px"],
    svg: {
      position: "relative",
      top: "3px",
    },
  },
  content: {
    width: "100%",
    alignItems: "center",
    textAlign: "left",
    pl: "3%",
  },
};
