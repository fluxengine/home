import React, { useState } from "react";
import SwiperCore, { Thumbs, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { Box, Container, Text, Heading } from "theme-ui";
import BlockTitle from "components/block-title";
import Image from "components/image";

import img1 from "assets/testi-1-1.jpeg";
import img2 from "assets/testi-1-2.jpeg";
import img3 from "assets/testi-1-3.jpeg";
import img4 from "assets/testi-1-4.jpeg";
import img5 from "assets/testi-1-5.jpeg";

SwiperCore.use([Thumbs, Autoplay]);

const TESTIMONIALS_DATA = [
  {
    image: img1,
    heading: "Steve Thompson",
    designation: "Global People Director",
    content:
      "David and his team of developers are highly talented. I'd go so far as to say the best in the business at building data platforms! They go the extra mile to ensure that solutions they deliver are best in class and that deadlines are met. I'd recommend them in a heartbeat!",
  },
  {
    image: img2,
    heading: "Natalia Sanz",
    designation: "Head of Technology",
    content:
      "Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster.",
  },
  {
    image: img3,
    heading: "Ece Akman",
    designation: "Senior Marketer",
    content:
      "Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal.",
  },
  {
    image: img4,
    heading: "Natalia Sanz",
    designation: "Head of Technology",
    content:
      "Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional.",
  },
  {
    image: img5,
    heading: "Ece Akman",
    designation: "Senior Marketer",
    content:
      "Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base.",
  },
];

const Testimonials = () => {
  // store thumbs swiper instance
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  const infoParams = {
    spaceBetween: 15,
    slidesPerView: 3,
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      576: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 60,
      },
    },
  };
  const contentParams = {
    spaceBetween: 0,
    slidesPerView: 1,
    autoHeight: true,
    autoplay: {
      delay: 5000,
    },
  };
  return (
    <Box as="section" id="testimonials" sx={styles.testimonials}>
      <Container>
        <BlockTitle
          slogan="Testimonials"
          title="Why customers love us"
          styles={styles.blockTitle}
        />
        <Swiper
          id="testimonialsContent"
          thumbs={{ swiper: thumbsSwiper }}
          {...contentParams}
        >
          {TESTIMONIALS_DATA.map((testimonialText, index) => (
            <SwiperSlide key={`testimonial-content-${index}`}>
              <Text sx={styles.testimonialsContent} as="p">
                {testimonialText.content}
              </Text>
            </SwiperSlide>
          ))}
        </Swiper>

        <Swiper
          id="testimonialsInfo"
          onSwiper={setThumbsSwiper}
          watchSlidesVisibility
          watchSlidesProgress
          {...infoParams}
        >
          {TESTIMONIALS_DATA.map((testimonial, index) => (
            <SwiperSlide key={`testimonial-info-${index}`}>
              <Image
                loading="lazy"
                width="50"
                height="50"
                src={testimonial.image}
                alt="testimonials image"
              />
              <Heading as="h3">{testimonial.heading}</Heading>
              <Text as="span">{testimonial.designation}</Text>
            </SwiperSlide>
          ))}
        </Swiper>
      </Container>
    </Box>
  );
};

export default Testimonials;

const styles = {
  testimonials: {
    pt: ["65px", null, null, null, null, "80px", "170px"],
    pd: "5%",
    ".blockTitle": {
      textAlign: "center",
      marginBottom: ["25px", null, null, null, null, "50px"],
    },
    "#testimonialsInfo": {
      textAlign: "center",
      marginTop: "30px",
      width: "100%",
      maxWidth: "820px",
      ".swiper-slide": {
        cursor: "pointer",
        borderTop: [
          "5px solid transparent",
          null,
          null,
          null,
          null,
          "5px solid transparent",
        ],
        position: "relative",
        paddingLeft: ["0", null, null, null, null, "0"],
        paddingTop: ["0", null, null, null, null, "0"],
        paddingBottom: ["0", null, null, null, null, "0"],
        minHeight: ["auto", null, null, null, null, "200px"],
        "&.swiper-slide-thumb-active": {
          borderColor: "#8D448B",
        },
      },
      img: {
        zoom: "200%",
        borderRadius: "50%",
        display: "block",
        marginLeft: "auto",
        marginRight: "auto",
        marginBottom: "15px",
        position: "relative",
        top: "auto",
        left: "auto",
        mt: "10px",
        transform: "translateY(0)",
      },
      h3: {
        fontSize: ["18px", null, 2, null, 3],
        lineHeight: 1,
        color: "black",
        display: "block",
      },
      span: {
        color: "text",
        opacity: "0.8",
        fontSize: [15, null, "15px"],
        lineHeight: 1,
        display: "block",
        display: "block",
        marginTop: "10px",
      },
    },
  },
  testimonialsContent: {
    margin: 0,
    fontSize: [2, null, 3, null, 4, "32px", 7],
    color: "black",
    lineHeight: [2.3, null, 1.8],
    textAlign: "center",
    fontFamily: "special",
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    maxWidth: "820px",
    marginTop: ["0", null, null, null, null, "-15px"],
  },
};
