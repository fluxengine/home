import React from "react";
import { ThemeUIProvider } from "theme-ui";
import { StickyProvider } from "contexts/app/app.provider";
import theme from "theme/index.js";
import SEO from "components/seo";
import Layout from "components/layout";

import Banner from "sections/banner";
import Services from "sections/services";
import Testimonials from "sections/testimonials";
import CustomerSupport from "sections/customer-support";
import VideoOne from "sections/video-one";
import BoostAgencies from "sections/boost-agencies";

export default function IndexPage() {
  return (
    <ThemeUIProvider theme={theme}>
      <StickyProvider>
        <Layout>
          <SEO description="" title="" />
          <Banner />
          <Services />
          <BoostAgencies />
          <VideoOne />
          <Testimonials />
          <CustomerSupport />
        </Layout>
      </StickyProvider>
    </ThemeUIProvider>
  );
}
